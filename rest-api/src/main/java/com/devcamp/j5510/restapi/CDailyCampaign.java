package com.devcamp.j5510.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Calendar;

@RestController
@CrossOrigin
public class CDailyCampaign {
    @GetMapping
    public String getMessage() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK); 
        switch (day) {
            case 1:
                return "Chủ nhật, tưng bừng combo Hải Sản";

            case 2:
                return "Thứ hai, mua 1 tặng 1";

            case 3:
                return "Thứ ba, combo 199 trở lại";

            case 4:
                return "Thứ tư, bánh mật ong tặng kem sữa chua";

            case 5:
                return "Thứ năm, mua 5 tặng 2";

            case 6:
                return "Thứ sáu, combo 99 199 299";

            case 7:
                return "Thứ bảy, cuối tuần rồi voucher free 150k";
        }
        return "Looking forward to weekend";
    }
}