"use strict";
$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// định nghĩa biến toán cục để lưu thông tin khách hàng
var gCustomerInfo = 
    {
      szCombo: null,
      pizzaType: "",
      loaiNuocUong: "",
      hoVaTen: "",
      email: "",
      dienThoai: "",
      diaChi: "",
      loiNhan: "",
      voucher:""
    };
// bạn có thể dùng để lưu trữ combo được chọn, 
// mỗi khi khách chọn menu S, M, L bạn lại đổi giá trị properties của nó
var gSelectedMenuStructure = {
        menuName: "...",    // S, M, L
        duongKinhCM: 0,
        suonNuong: 0,
        saladGr: 0,
        drink: 0,
        priceVND: 0
    }
// bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
var gSelectedPizzaType = "..." 
//biến lưu phần trăm giảm giá
var gDiscount = 0
//biến lưu giá sau giảm
var gPriceAfterDiscount = 0 

  const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
  const gBASE_URLDRINKS = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
  var gID = ""; //biến để lưu Id và orderID khi create thành công order
  var gORDER_ID = "";

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading()
$("#btn-small").click(function(){onBtnSmallClick()})
$("#btn-medium").click(function(){onBtnMediumClick()})
$("#btn-large").click(function(){onBtnLargeClick()})
$("#btn-haisan").click(function(){onBtnHaiSanClick()})
$("#btn-hawaii").click(function(){onBtnHawaiiClick()})
$("#btn-bacon").click(function(){onBtnBaconClick()})
$("#btn-kiemtradon").click(function(){onBtnKiemTraDonClick()})
$("#btn-create-order").click(function(){onBtnGuiDonClick()})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm thực thi khi load trang
function onPageLoading() {
  "use strict"
  console.log("Data Drinks is loading...");
  loadDataToSelectDrink()
  loadingPromotionMessage()
}

//hàm thực thi khi nút chọn size S được Click
function onBtnSmallClick() {
 var vSelectedMenuStructure = getSize("Small", 20, 2, 200, 2, 150000);   //tạo một đối tượng gói dịch vụ , được tham số hóa
 vSelectedMenuStructure.displayInConsoleLog();   // gọi method hiển thị thông tin
 changeButtonChooseSizeColor("Small"); //đổi màu button
 gSelectedMenuStructure = vSelectedMenuStructure;
}

//hàm thực thi khi nút chọn size M được Click
function onBtnMediumClick() {
 var vSelectedMenuStructure = getSize("Medium", 25, 4, 300, 3, 200000);   //tạo một đối tượng gói dịch vụ , được tham số hóa
 vSelectedMenuStructure.displayInConsoleLog();   // gọi method hiển thị thông tin
 changeButtonChooseSizeColor("Medium"); //đổi màu button
 gSelectedMenuStructure = vSelectedMenuStructure;
}
 //hàm thực thi khi nút chọn size L được Click
function onBtnLargeClick() {
 var vSelectedMenuStructure = getSize("Large", 30, 8, 500, 4, 250000);   //tạo một đối tượng gói dịch vụ , được tham số hóa
 vSelectedMenuStructure.displayInConsoleLog();   // gọi method hiển thị thông tin
 changeButtonChooseSizeColor("Large"); //đổi màu button
 gSelectedMenuStructure = vSelectedMenuStructure;
}
//hàm thực thi khi click chọn loại pizza Hawaii
function onBtnHawaiiClick() {
  var vSelectedPizzaType = "Hawaii";
  changeBtnPizzaTypeColor("Hawaii");
  console.log("%cPizza được chọn: " + vSelectedPizzaType, "color: blue");
  gSelectedPizzaType = vSelectedPizzaType;
}
//Hàm thực thi khi click chọn pizza Hải sản
function onBtnHaiSanClick() {
  var vSelectedPizzaType = "Hải Sản";
  changeBtnPizzaTypeColor("Haisan");
  console.log("%cPizza được chọn: " + vSelectedPizzaType, "color: blue");
  gSelectedPizzaType = vSelectedPizzaType;
}
//Hàm thực thi khi click chọn pizza Bacon
function onBtnBaconClick() {
  var vSelectedPizzaType = "Thịt xông khói (Bacon)";
  changeBtnPizzaTypeColor("Bacon");
  console.log("%cPizza được chọn: " + vSelectedPizzaType, "color: blue");
  gSelectedPizzaType = vSelectedPizzaType;
}

//khi nút Kiểm tra đơn được nhấn
function onBtnKiemTraDonClick() {
  console.log("%c Kiểm tra đơn", "color: orange;");
  //thu thập dữ liệu
  gCustomerInfo = docDuLieuWebVaoObj();
  kiemTraVoucher(gCustomerInfo.voucher)
  //kiểm tra dữ liệu
  if(kiemTraThongTinObj(gCustomerInfo) == true) {
  //3. Xử lý hiển thị
   hienThiDuLieuDatHang(gCustomerInfo);
  }
}

//khi nút gửi đơn được Click, các thông tin đặt hàng ghi vào Console;
    function onBtnGuiDonClick() {
        "use strict";
        // khai báo object order để chứa thông tin đặt hàng với 14 thuộc tính
        var vObjectRequest = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        }
        //Thu thập thông tin vào vObjectRequest
        thuThapThongTinCreateOrder(vObjectRequest);
        //Call API create order
        $.ajax({
          url: gBASE_URL,
          type:"POST",
          data: JSON.stringify(vObjectRequest),
          contentType:"application/json;charset=UTF-8",
          success: function(res) {
                console.log(res);
                gORDER_ID = res.orderId;
                gID = res.id;
                handleDisplayAfterCallAPICreateOrder(res)
          },
          error: function(error) {
                alert(error.responseText);
          }
        })
      }
  
  //Khi ấn orderDetail trên navigation
  function onNavOrderDetailClick() {
    var vOrderDetailUrl= "viewOrder.html?id=" + gID + "&OId=" + gORDER_ID;
    window.location.href = vOrderDetailUrl;
  }
     
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//load promotion message
function loadingPromotionMessage() {
  console.log("Promotion message")
  $.ajax({
    url: "http://localhost:8080/campaigns/",
    type: "GET",
    success: function(res) {
      console.log(res);
      $("#promotion-message").html(res)
    },
    error: function(ajax) {
      console.log("no ok")
      console.log(ajax.responseText)
    }
  })
}

//hàm load data drink list (danh sách loại nước uống) về
function loadDataToSelectDrink() {
    "use strict";
    $.ajax({
        url: gBASE_URLDRINKS,
        type: "GET",
        dataType: "JSON",
        success: function(resDrinkOj){
            //dùng vòng lặp for để đi qua từng phần tử
            for (var bI = 0; bI<resDrinkOj.length; bI++) {
                $("#select-drink").append($('<option>', {
                    value: resDrinkOj[bI].maNuocUong,
                    text: resDrinkOj[bI].tenNuocUong
                }));
            }
        },
        error: function(ajaxContext) {
            console.log(ajaxContext.responseText)
        }
    })
}
//hàm lấy thông tin sz combo pizza được chọn
function getSize(paramSize, paramDuongKinh, paramSuonNuong, paramSalad, paramDrink, paramPriceVND) {
  var vSelectedCombo = {
    menuName: paramSize,
    duongKinhCM: paramDuongKinh,
    suonNuong: paramSuonNuong,
    saladGr: paramSalad,
    drink: paramDrink,
    priceVND: paramPriceVND,
    displayInConsoleLog() { // method display combo infor - phương thức hiển thị combo pizza lựa chọn
      console.log("%cCOMBO SIZE SELECTED ..........", "color:blue");
      console.log(this.menuName);  //this = "đối tượng này"
      console.log("Đường kính (Cm): " + this.duongKinhCM);
      console.log("Sườn nướng (Cây): " + this.suonNuong); 
      console.log("Salad (Gr): " + this.saladGr);
      console.log("Nước ngọt: " + this.drink);
      console.log("Đơn giá VND: " + this.priceVND);
    }
  }
    return vSelectedCombo;  //trả lại đối tượng, có đủ dữ liệu (attribute) và các methods (phương thức)
}
//Đổi màu cho nút combo được chọn
function changeButtonChooseSizeColor(paramChooseSize) {
  var vBtnSmall = $("#btn-small");  // truy vấn nút chọn sz S
  var vBtnMedium = $("#btn-medium"); //truy vấn nút chọn sz M 
  var vBtnLarge = $("#btn-large"); //truy vấn nút chọn sz L
  
  if (paramChooseSize === "Small") {  //nếu chọn Small thì thay màu nút Small bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    vBtnSmall.removeClass().addClass("btn btn-menu-selected");
    vBtnMedium.removeClass().addClass("btn btn-menu");
    vBtnLarge.removeClass().addClass("btn btn-menu");
  }
  else if (paramChooseSize === "Medium") {  //nếu chọn Medium thì thay màu nút Medium bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    vBtnSmall.removeClass().addClass("btn btn-menu");
    vBtnMedium.removeClass().addClass("btn btn-menu-selected");
    vBtnLarge.removeClass().addClass("btn btn-menu");
  } 
  else if (paramChooseSize === "Large") { //nếu chọn Large thì thay màu nút Large bàng màu cam, hai nút kia xanh
    vBtnSmall.removeClass().addClass("btn btn-menu");
    vBtnMedium.removeClass().addClass("btn btn-menu");
    vBtnLarge.removeClass().addClass("btn btn-menu-selected");
  }
}
//Đổi màu cho nút pizza type được chọn
function changeBtnPizzaTypeColor (paramType) {
  var vBtnHawaii = $("#btn-hawaii");
  var vBtnHaiSan = $("#btn-haisan");
  var vBtnBacon = $("#btn-bacon");
  
  if(paramType == "Hawaii") {
    vBtnHawaii.removeClass().addClass("btn btn-menu-selected");
    vBtnHaiSan.removeClass().addClass("btn btn-menu");
    vBtnBacon.removeClass().addClass("btn btn-menu");
  }
  if(paramType == "Haisan") {
    vBtnHawaii.removeClass().addClass("btn btn-menu");
    vBtnHaiSan.removeClass().addClass("btn btn-menu-selected");
    vBtnBacon.removeClass().addClass("btn btn-menu");
  }
  if(paramType == "Bacon") {
    vBtnHawaii.removeClass().addClass("btn btn-menu");
    vBtnHaiSan.removeClass().addClass("btn btn-menu");
    vBtnBacon.removeClass().addClass("btn btn-menu-selected");
  }
}

//hàm đọc dữ liệu từ web và chuyển dữ liệu vào Obj paramThongTinDangKyObj
function docDuLieuWebVaoObj() {
  var vSelectDrinkText = $("#select-drink option:selected").text();
  var vInputHoVaTen = $("#inp-fullname").val().trim();
  var vInputEmail = $("#inp-email").val().trim();
  var vInputDienThoai = $("#inp-dien-thoai").val().trim();
  var vInputDiaChi = $("#inp-dia-chi").val().trim();
  var vInputLoiNhan = $("#inp-message").val().trim();
  var vInputVoucherId = $("#inp-magiamgia").val().trim();
  
var vCustomerInfo = {
        szCombo: gSelectedMenuStructure,
        pizzaType: gSelectedPizzaType,
        loaiNuocUong:vSelectDrinkText,
        hoVaTen: vInputHoVaTen,
        email: vInputEmail,
        dienThoai: vInputDienThoai,
        diaChi: vInputDiaChi,
        loiNhan: vInputLoiNhan,
        voucher: vInputVoucherId
      }
  return vCustomerInfo;
}   

function kiemTraVoucher (paramVoucher) {
  if(gCustomerInfo.voucher == ""){
    gDiscount = 0;
    gPriceAfterDiscount = gCustomerInfo.szCombo.priceVND;
  }
  if(gCustomerInfo.voucher!="") { //nếu có điền voucher ID thì kiểm tra
      //Sử dụng Ajax để gửi request lên server
      $.ajax({
          url: `http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/${gCustomerInfo.voucher}`,
          type: "GET",
          dataType: "JSON",
          async: false,
          success: function(resVoucherObj){
              console.log(resVoucherObj)
              gDiscount = resVoucherObj.phanTramGiamGia;
              gPriceAfterDiscount = gCustomerInfo.szCombo.priceVND * (1 - gDiscount / 100);
          },
          error: function(ajaxContext){
              console.log(ajaxContext.responseText)
              gDiscount = 0;
              alert ("Mã giảm giá không tồn tại!")
              gPriceAfterDiscount = gCustomerInfo.szCombo.priceVND;
          }
      })
  }
}

//kiểm tra thông tin mua hàng
  function kiemTraThongTinObj(paramThongTinObj) {
  //debugger;
    if(paramThongTinObj.szCombo.menuName == "...") {
      alert ("Chưa chọn Size Combo");
      return false;
    }
    if(paramThongTinObj.pizzaType == "...") {
      alert ("Chưa chọn Pizza Type");
      return false;
    }
    if(paramThongTinObj.loaiNuocUong === "Tất cả loại nước uống ") {
      alert ("Chưa chọn Loại thức uống");
      return false;
    }
    if(paramThongTinObj.hoVaTen == "") {
      alert("Họ và Tên chưa được nhập!");
      return false;
    }
    if(paramThongTinObj.email == "") {
      alert("Email chưa được nhập!");
      return false;
    }
    if(validateEmail(paramThongTinObj.email) == false){
        alert("Email không hợp lệ, vui lòng nhập lại");
        return false;
    }
    if(paramThongTinObj.dienThoai == "") {
      alert("Điện thoại chưa được nhập!");
      return false;
    }
    if(paramThongTinObj.diaChi == "") {
      alert("Địa chỉ chưa được nhập!");
      return false;
    }
    return true;
}

//kiểm tra email có hợp lệ không
    function validateEmail(paramEmail) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(paramEmail);
    }

// hàm thực hiện việc hiển thị dữ liệu đặt hàng lên web
    function hienThiDuLieuDatHang(paramThongTinObj) {
        console.log(gPriceAfterDiscount,gDiscount,gCustomerInfo)
        
        $("#input-fullname").val(paramThongTinObj.hoVaTen)
        $("#input-phone").val(paramThongTinObj.dienThoai)
        $("#input-address").val(paramThongTinObj.diaChi)
        $("#input-message").val(paramThongTinObj.loiNhan)
        $("#input-voucher").val(paramThongTinObj.voucher)

        var vOrderDetails = `Xác nhận ${paramThongTinObj.hoVaTen}, Điện thoại: ${paramThongTinObj.dienThoai}, Địa chỉ: ${paramThongTinObj.diaChi}, Combo menu ${paramThongTinObj.szCombo.menuName}, Sườn nướng: ${paramThongTinObj.szCombo.suonNuong}, Salad(Gr): ${paramThongTinObj.szCombo.saladGr}, Nước ngọt: ${paramThongTinObj.szCombo.drink}, Loại Thức Uống: ${gCustomerInfo.loaiNuocUong}, Loại Pizza: ${paramThongTinObj.pizzaType}, Giá: ${paramThongTinObj.szCombo.priceVND} VNĐ, Phải thanh toán: ${gPriceAfterDiscount} VNĐ (giảm giá ${gDiscount} %) `
        $("#input-orderinfo").val(vOrderDetails)
        $("#create-order-modal").modal("show")
    }
           
//hàm thu thập thông tin để gọi api create order
    function thuThapThongTinCreateOrder(paramObjectRequest) {
        paramObjectRequest.kichCo = gCustomerInfo.szCombo.menuName;
        paramObjectRequest.duongKinh = gCustomerInfo.szCombo.duongKinhCM;
        paramObjectRequest.suon = gCustomerInfo.szCombo.suonNuong;
        paramObjectRequest.salad = gCustomerInfo.szCombo.saladGr;
        paramObjectRequest.loaiPizza = gCustomerInfo.pizzaType;
        paramObjectRequest.idVourcher = gCustomerInfo.voucher;
        paramObjectRequest.idLoaiNuocUong = gCustomerInfo.loaiNuocUong;
        paramObjectRequest.soLuongNuoc = gCustomerInfo.szCombo.drink;
        paramObjectRequest.hoTen = gCustomerInfo.hoVaTen;
        paramObjectRequest.thanhTien = gCustomerInfo.szCombo.priceVND;
        paramObjectRequest.email = gCustomerInfo.email;
        paramObjectRequest.soDienThoai = gCustomerInfo.dienThoai;
        paramObjectRequest.diaChi = gCustomerInfo.diaChi;
        paramObjectRequest.loiNhan = gCustomerInfo.loiNhan;
    }

//hàm xử lý hiển thị sau khi call API create order
    function handleDisplayAfterCallAPICreateOrder(paramCreateOrder) {
      $("#create-order-modal").modal("hide")
      $("#inp-orderid").val(gORDER_ID);
      $("#thankyou-modal").modal("show")
    }
})

    